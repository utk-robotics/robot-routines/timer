#include <chrono>
#include <ctime>
#include <iostream>
#include <rip/logger.hpp>
#include <rip/routines/timer_routine.hpp>

namespace rip::routines
{
    TimerRoutine::TimerRoutine(const nlohmann::json &config,
                               std::shared_ptr< EventSystem > es,
                               std::string id,
                               CompTable comps)
        : Routine(config, es, id, comps)
    {
        try
        {
            // access to the data
            m_time_out = config.at("elapsed_time");
        }
        catch (nlohmann::json::type_error &err_to)
        {
            throw InvalidConfig("Could not find this command in the config file");
        }
        catch (nlohmann::json::out_of_range &err_to_t)
        {
            throw InvalidConfig("Could not find this command in the config file");
        }
    }

    void TimerRoutine::start(std::vector< std::any > data)
    {
        // called as soon the object is created
        m_start = std::chrono::system_clock::now();
        // std::cout << "Timer started\n";
        m_running = true;
        run();
        // all the actual logic of the routine
    }


    void TimerRoutine::stop(std::vector< std::any > data)
    {
        //  std::cout << "Stop routine called\n";
        m_running = false;
    }


    void TimerRoutine::saveComponents(CompTable comps)
    {
    }


    void TimerRoutine::run()
    {
        double elapse_t = -1.0;

        while (m_running && (elapse_t < m_time_out))
        {
            auto end = std::chrono::system_clock::now();

            std::chrono::duration< double > elapsed_seconds = end - m_start;

            // std::time_t end_time = std::chrono::system_clock::to_time_t(end);
            // std::cout << "finished computation at " << std::ctime(&end_time)
            // << "elapsed time until now: " << elapsed_seconds.count() << "s\n";

            elapse_t = elapsed_seconds.count();
            //  std::cout << "Elapsed time is = " << elapse_t << std::endl;
        }


        sendMessage("timer_finished");
        stop();
    }


    void TimerRoutine::handle_subscription(std::string handle, std::string func_id, std::string routine_id)
    {
    }

}  // namespace rip::routines
