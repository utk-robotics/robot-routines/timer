#ifndef TIMER_ROUTINE_HPP
#define TIMER_ROUTINE_HPP

#include <rip/routine.hpp>

namespace rip::routines
{
    class TimerRoutine : public Routine
    {
    public:
        TimerRoutine(const nlohmann::json &config, std::shared_ptr< EventSystem > es, std::string id, CompTable comps);

        virtual void start(std::vector< std::any > data = {}) override;

        virtual void stop(std::vector< std::any > data = {}) override;

    protected:
        virtual void saveComponents(CompTable comps) override;

        virtual void run() override;

        virtual void handle_subscription(std::string handle, std::string func_id, std::string routine_id = "") override;

        double m_time_out;
        std::chrono::system_clock::time_point m_start;
    };

}  // namespace rip::routines

#endif
